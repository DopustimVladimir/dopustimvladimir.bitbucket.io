
// Synthesizer
class Synthesizer {

    static noteNamesList = [ "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" ]

    static notes = Synthesizer.buildNotes()

    constructor() {
        this.audioContext = new AudioContext()
        this.oscList = [] // Array(10).fill({})
    }

    setVolume(volume) {
        this.volume = volume
    }

    setType(type) {
        this.type = type
    }

    playNote(note, velocity) {
        console.log(note.name + note.octave)
        if (note) {
            const osc = new OscillatorNode(this.audioContext, {
                type: this.type,
                frequency: note.frequency
            })
            const audioGain = new GainNode(this.audioContext, {
                gain: (velocity ? velocity / 127 : this.volume)
            })
            audioGain.connect(this.audioContext.destination)
            osc.connect(audioGain)
            osc.start()
            if (!(note.octave in this.oscList))
                this.oscList[note.octave] = {}
            if (!(note.name in this.oscList[note.octave]))
                this.oscList[note.octave][note.name] = {}
            this.oscList[note.octave][note.name].osc = osc
            this.oscList[note.octave][note.name].gain = audioGain
        } else {
            console.error("Error: Could not recognize note!")
        }
    }

    stopNote(note) {
        if (note) {
            this.oscList[note.octave][note.name].osc.stop()
            delete this.oscList[note.octave][note.name]
        } else {
            console.error("Error: Could not recognize note!")
        }
    }

    getNote(octave, name) {
        return Synthesizer.notes.find((it) => it.octave === Number(octave) && it.name === name)
    }

    getNoteFromMIDI(midi) {
        return Synthesizer.notes.find((it) => it.midi === Number(midi))
    }

    static buildNotes() {
        const notes = []
        const names = Synthesizer.noteNamesList
        for (let i = 0, midi = 12; i < 10; i++)
            for (const name of names) {
                if (midi >= 21 && midi <= 108) // A0..C7 (Piano)
                    notes.push({ octave: i, name: name, midi: midi,
                        frequency: 2 ** ((midi - 69) / 12) * 440 })
                midi++
            }
        return notes
    }
}

// eslint-disable-next-line max-lines-per-function
function main() {
    const keyboard = document.querySelector(".synth__keyboard")
    const keys = keyboard.querySelectorAll(".synth__key")
    const waveformControl = document.querySelector("#waveform")
    const volumeControl = document.querySelector("#volume")

    const synth = new Synthesizer()

    // Keyboard
    keyboard.addEventListener("mousedown", onMouseDown)
    keyboard.addEventListener("mouseup", onMouseUp)

    // Waveform Control
    waveformControl.addEventListener("change", () => {
        synth.setType(waveformControl.options[waveformControl.selectedIndex].value)
    })
    synth.setType(waveformControl.options[waveformControl.selectedIndex].value)

    // Volume Control
    volumeControl.addEventListener("change", () => {
        synth.setVolume(volumeControl.value)
    })
    synth.setVolume(volumeControl.value)

    // MIDI
    if (navigator.requestMIDIAccess) {
        navigator.requestMIDIAccess().then(onMIDISuccess, () => {
            uiNotify("Не удалось получить доступ к MIDI-контроллеру")
            console.error("Error: Failed to access MIDI device!")
        })
    } else {
        uiNotify("В этом браузере MIDI не поддерживается")
        console.error("Error: Web MIDI API is not supported!")
    }

    function onMouseDown(ev) {
        if (ev.target.classList.contains("synth__key"))
            if (!ev.target.classList.contains("synth__key--active")) {
                const note = synth.getNote(ev.target.dataset.octave, ev.target.dataset.note)
                synth.playNote(note)
                ev.target.classList.add("synth__key--active")
            }
    }

    function onMouseUp(ev) {
        if (ev.target.classList.contains("synth__key"))
            if (ev.target.classList.contains("synth__key--active")) {
                const note = synth.getNote(ev.target.dataset.octave, ev.target.dataset.note)
                synth.stopNote(note)
                ev.target.classList.remove("synth__key--active")
            }
    }

    function onMIDISuccess(ev) {
        for (const input of ev.inputs.values()) {
            input.onmidimessage = onMIDIMessage
            console.log(input.manufacturer) // AKAI
            console.log(input.name) // MPK mini 3
            uiNotify(`Вам доступен MIDI-контроллер ${input.name} от ${input.manufacturer}`)
        }
    }

    function onMIDIMessage(ev) {
        const command = ev.data[0]
        const value = ev.data[1]
        const velocity = ev.data.length > 2 ? ev.data[2] : 0
        const note = synth.getNoteFromMIDI(value)
        switch (command) {
            case 144:
                if (velocity > 0) {
                    synth.playNote(note, velocity)
                    uiActivateKey(note)
                } else {
                    synth.stopNote(note)
                    uiDeactivateKey(note)
                }
                break
            case 128:
                synth.stopNote(note)
                uiDeactivateKey(note)
                break
        }
    }

    function uiCreateNotificationElement(text) {
        const e = document.createElement("div")
        e.classList.add("notifications__item")
        e.textContent = text
        return e
    }

    function uiActivateKey(note) {
        for (const key of keys)
            if (Number(key.dataset.octave) === note.octave && key.dataset.note === note.name) {
                key.classList.add("synth__key--active")
                break
            }
    }

    function uiDeactivateKey(note) {
        for (const key of keys)
            if (Number(key.dataset.octave) === note.octave && key.dataset.note === note.name) {
                key.classList.remove("synth__key--active")
                break
            }
    }

    function uiNotify(text) {
        const notifications = document.querySelector(".notifications")
        const e = uiCreateNotificationElement(text)
        notifications.appendChild(e)
        setTimeout(() => {
            notifications.removeChild(e)
        }, 2000)
    }
}

addEventListener("DOMContentLoaded", () => {

    const darkModeCheckbox = document.getElementById("darkmode")

    function updateTheme() {
        darkModeCheckbox.checked
            ? document.body.classList.add("dark-mode")
            : document.body.classList.remove("dark-mode")
    }
    darkModeCheckbox.addEventListener("change", () => {
        updateTheme()
    })
    updateTheme()
    main()
})
